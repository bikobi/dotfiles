# Polybar

## Directory structure

```
.
├── config.ini
├── launch.sh
├── modules
│   ├── alsa.ini
│   ├── battery.ini
│   └── ...
└── themes
    └── gruvbox-dark.ini
```

The file `config.ini` is the entry point; it contains the configuration and settings for the bars (in my case, there's only one bar). It also includes the file `themes/gruvbox-dark.ini` and the directory `modules/`.

The directory `themes/` contains the colorschemes; each file defines a `[colors]` section with the colors' names and values. To use a different colorscheme, simply include another file in `config.ini`.

The directory `modules/` contains a file for each module, which can be referenced in `config.ini`; I find it much neater this way.

The script `launch.sh` is used to launch Polybar; it terminates any running instances and takes care of the logging.

## How to decorate the bar

![rounded module groups](./polybar_round_edge_layer.png)

The bars in the image show the modules on the right and the modules on the left as if they were in a different "layer", with a different background color and a round edge.

To achieve the look in the image:

### 1. Create the modules `left` and `right`

These modules are of type `custom/text` and they contain the icons of the right- and left-facing semi-circle. That's right, the round edge is actually just another module displaying a single semi-circle character.

These modules must use a font with a bigger size to overflow the bar's height, so you will need to define at least two fonts: one for these modules and one for everything else. They can be the same font-family, just with different sizes and offsets.

Then, in your config:

```ini
; Add module/right to the end of the modules on the left.
modules-left = your-module-1 your-module-2 right
; Add module/left at the beginning of the modules on the right.
modules-right = left your-module-3 your-model-4
```

See `modules/right.ini` and `modules/left.ini` to see how I did it.

### 2. Remove margins and padding

In your bar, set `padding-right`, `padding-left` and `module-margin` to `0`.

### 3. Remove the separator

In your bar, remove or comment out the `separator = " "` line.

### 4. Assign the background colors

Give your bar a background color, let's say *bg*.  
Then, pass all the modules on the left and assign *another* background color to each; let's say *bg-alt*.  
Assign *bg-alt* as the background color of all the modules on the right too.

To do this, you will need to edit different attributes for different modules; mostly, you'll need to set `format-background` and `label-background`, but if you set different formats or labels for a single module, you'll need to set the background of each. For example:

```ini
[module/battery]

format-charging = ...
format-charging-background = ${colors.bg-alt}

format-discharging = ...
format-discharging-background = ${colors.bg-alt}

; and so on for each format or label defined.
```

### 5. Fix the spacing

Since we removed any spacing between the modules, it's likely that your bar now looks all cramped and ugly. To reestabilish some nice spacing without breaking the new design, you have to work with the labels of the modules, inserting literal space characters " ".

### 6. Conclusions

This design looks good, but if you decide to use it be aware that the config for it is not very elegant, and changing the position or order of the modules breaks it easily.
