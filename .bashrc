# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
# if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
# then
#     PATH="$HOME/.local/bin:$HOME/bin:$PATH"
# fi
# export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

# --------------------- #
# ~/.bashrc

# limit recursive functions, see `man bash`
[[ -z "$FUNCNEST" ]] && export FUNCNEST=100 

# set Vim mode
set -o vi

## Aliases
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias cl='clear'
alias tree='tree -L 3'
alias ':q'='exit'

## NNN configuration
BLK="0B" CHR="0B" DIR="04" EXE="06" REG="00" HARDLINK="06" SYMLINK="06" MISSING="00" ORPHAN="09" FIFO="06" SOCK="0B" OTHER="06"
export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER" # nord theme
export NNN_OPENER=/home/bbko/.config/nnn/plugins/nuke
alias nnn='nnn -c' # open with CLI by default through nuke

## Use the up and down arrow keys for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

## Useful functions
cdls(){ cd "$@" && ls -A; }
mkdircd(){ mkdir "$1" && cd "$1" ; }
md2pdf(){ pandoc --table-of-contents --number-sections --pdf-engine=xelatex -V geometry:"top=2.5cm, bottom=1.5cm, left=2.5cm, right=2.5cm" -V documentclass=report -o "$2" "$1" --toc -V urlcolor=NavyBlue; }
 
## Exports
export PS1="[\u@\h \[\033[37;2m\]\W\[\033[37;0m\]]\$ "

export MANPAGER='nvim +Man!' # use Neovim as manpager
export EDITOR='/usr/bin/nvim'

# --------------------- #
