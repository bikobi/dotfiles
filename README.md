# Gruvbox-inspired dotfiles

![screenshot](./images/screenshot.png)

|program|description|
|---|---|
|bspwm|window manager|
|sxhkd|hotkey daemon|
|polybar|status bar|
|dmenu|menu|

|info||
|----|---|
|JetBrainsMono Nerd Font|font|
|gruvbox|colors|


I strived to create a configuration that is visually appealing (to my own taste) while remaining *fairly simple* and easy to update. This influenced some choices, including:
- Using only one font across the rice (managing fonts and glyphs, especially in Polybar, is a pain in the butt);
- Choosing dmenu over rofi (rofi's configuration options are way overkill for my needs);
- Choosing herbe over dunst: herbe doesn't require a daemon (but you can use [tiramisu](https://github.com/Sweets/tiramisu) if you need one) and has much simpler configuration.

## If you want to use my dotfiles

If you're going to use my dotfiles, keep in mind that every detail of this rice was fine-tuned to fit *my own* needs and taste; you will surely want to adjust it to *your* preference. To facilitate the process, I try to keep the files well organized and documented with comments and READMEs.  
In particular, I recommend that you do the following:

1. Check `.bashrc`, as it includes some configuration that might be unnecessary or confusing to you (this includes [nnn](https://github.com/jarun/nnn) config, the `md2pdf()` function, some Neovim config and more).
2. Go through `.config/sxhkd/sxhkdrc` and make sure that you update the commands to use the software you want (browser, terminal emulator), or to install the software that provides the commands I use (`brightnessctl`, `amixer`). More on these in [Dependencies](#dependencies).
3. Update `.gitconfig` to use *your* username and email.
4. Install *JetBrainsMono Nerd Font* [**from here**](https://nerdfonts.com/font-downloads) to guarantee that the glyphs work.
5. Be aware that `themes/` and `icons/` directories' contents should go under the corresponding `/usr/share/{themes,icons}` directories.

You should also know that my **Neovim dotfiles** are in a separate repo, [here](https://gitlab.com/bikobi/neovim-config).

## Dependencies

Below is a non-comprehensive list of extra software that might be necessary for the rice to work properly. See also `packages.txt` for a more comprehensive list of software I use.

|program|description|
|---|---|
|imagemagick|capture screenshots with `import`|
|brightnessctl|adjust screen brightness|
|alsa|adjust volume with `amixer`|
|xorg-xsetroot|fix the cursor theme|
|feh|set background|

## Credits 

It should be noted that most of this rice is made by adapting and piecing together parts of works by others; I try to link the resources and inspirations I used wherever relevant, and below.

- [Colors](https://github.com/morhetz/gruvbox)
- [Wallpaper](https://www.reddit.com/r/unixporn/comments/m17ajg/oc_gruvboxfactory_your_gruvbox_themed_wallpaper/)
- [Polybar's rounded layers inspiration](https://github.com/edu-flores/linux-dotfiles)
- [Terminal colors script](https://gitlab.com/dwt1/shell-color-scripts)

I don't remember where I got the GTK theme and icons from.
